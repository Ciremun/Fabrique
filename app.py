#!/usr/bin/env python3

import os
from datetime import datetime
from threading import Thread

from flask import Flask, request, redirect, make_response
from gevent.pywsgi import WSGIServer
from dotenv import load_dotenv

load_dotenv()

import source.db as db
from source.form import FormHandler, Cookie
from source.session import hash_password, verify_password, get_session, authorized_endpoint
from source.mailing_list import mailing_list_handler, notification_handler, verify_mailing_list
from source.log import logger
from source.client import verify_client_and_get_operator_code


app = Flask(__name__)
app.secret_key = os.environ.get('SECRET_KEY')

assert app.secret_key is not None

mailing_list_handler_thread = Thread(target=mailing_list_handler, daemon=True)
mailing_list_handler_thread.start()


notification_handler_thread = Thread(target=notification_handler, daemon=True)
notification_handler_thread.start()


@app.route('/add_client', methods=['POST'])
@authorized_endpoint
def add_client(handler):
    handler.message['operator_code'] = verify_client_and_get_operator_code(handler)
    client_id = db.add_client(**handler.message)
    logger.info(f'add client: {handler.message}, client_id={client_id}')
    return handler.make_response(status=200, client_id=client_id)


@app.route('/update_client', methods=['PUT'])
@authorized_endpoint
def update_client(handler):
    handler.message['operator_code'] = verify_client_and_get_operator_code(handler)
    db.update_client(**handler.message)
    logger.info(f'update client: {handler.message}')
    return handler.make_response(status=200)


@app.route('/delete_client/<int:client_id>', methods=['DELETE'])
@authorized_endpoint
def delete_client(handler, client_id):
    db.delete_client(client_id)
    logger.info(f'delete client: client_id={client_id}')
    return handler.make_response(status=200)


@app.route('/add_mailing_list', methods=['POST'])
@authorized_endpoint
def add_mailing_list(handler):
    verify_mailing_list(handler)
    mailing_list_id = db.add_mailing_list(**handler.message)
    return handler.make_response(status=200, mailing_list_id=mailing_list_id)


@app.route('/update_mailing_list', methods=['PUT'])
@authorized_endpoint
def update_mailing_list(handler):
    verify_mailing_list(handler)
    db.update_mailing_list(**handler.message)
    return handler.make_response(status=200)


@app.route('/delete_mailing_list/<int:ml_id>', methods=['DELETE'])
@authorized_endpoint
def delete_mailing_list(handler, ml_id):
    db.delete_mailing_list(ml_id)
    return handler.make_response(status=200)


@app.route('/mailing_list_stats', methods=['GET'])
@authorized_endpoint
def mailing_list_stats(handler):
    stats = db.mailing_list_stats()
    return handler.make_response(status=200, stats=stats)


@app.route('/mailing_list_details/<int:ml_id>', methods=['GET'])
@authorized_endpoint
def mailing_list_details(handler, ml_id):
    details = db.mailing_list_details(ml_id)
    return handler.make_response(status=200, details=details)


@app.route('/login', methods=['POST'])
def login():
    handler = FormHandler(request, '/login')
    handler.get_form()

    username = handler.message.get('username')
    password = handler.message.get('password')

    if not all(isinstance(x, str) for x in [username, password]):
        return handler.make_response(message='Ошибка: введите имя пользователя и пароль', status=400)

    if not all(0 < len(x) <= 25 for x in [username, password]):
        return handler.make_response(message='Ошибка: имя пользователя и пароль от 1 до 25 символов', status=400)

    for i in username, password:
        for letter in i:
            code = ord(letter)
            if (33 <= code <= 57) or (65 <= code <= 122):
                continue
            return handler.make_response(message='Ошибка: только английские символы и числа', status=400)

    userinfo = db.get_user(username)
    if userinfo:
        user_id, hashed_pwd, *_ = userinfo
        if verify_password(hashed_pwd, password):
            SID = hash_password(app.secret_key)
            db.add_session(SID, user_id, datetime.now())
            logger.info(f"login user: user_id={user_id}, username={username}")
            return handler.make_response(cookie=Cookie('SID', SID), status=200)
        else:
            return handler.make_response(message='Ошибка: неверный пароль', status=400)
    else:
        hashed_pwd = hash_password(password)
        SID = hash_password(app.secret_key)
        date = datetime.now()
        user_id = db.add_user(username, hashed_pwd, 'admin', date)
        db.add_session(SID, user_id, date)
        logger.info(f"register user: user_id={user_id}, username={username}")
        return handler.make_response(cookie=Cookie('SID', SID), status=200)


@app.route('/logout', methods=['POST'])
def logout():
    session_id = request.cookies.get('SID')
    session = get_session(session_id)
    response = make_response()
    if session:
        db.delete_session(session_id)
        response.set_cookie('SID', '', expires=0, secure=request.is_secure)
    return response



@app.route('/docs', methods=['GET'])
def docs():
    return redirect('https://app.swaggerhub.com/apis-docs/CIREMUN_1/api/1.0.0')


def main():
    wsgi = WSGIServer(('0.0.0.0', int(os.environ.get('PORT'))), app)
    wsgi.serve_forever()


if __name__ == '__main__':
    main()
