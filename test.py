#!/usr/bin/env python3

from app import main
import os
import unittest
from threading import Thread

import psycopg2
import requests as rq
from dotenv import load_dotenv

load_dotenv()

conn = psycopg2.connect('postgres://postgres:password@localhost:5432/postgres')
conn.autocommit = True
cursor = conn.cursor()
cursor.execute('CREATE DATABASE ml_test;')
os.environ['DATABASE_URL'] = 'postgres://postgres:password@localhost:5432/ml_test'


client_payload = {
    'phone_number': '79650000000',
    'tag': 'Orange',
    'timezone': 'Europe/Moscow'
}

client_payload_with_id = {'client_id': 1}
client_payload_with_id.update(client_payload)

ml_payload = {
    'start_time': '2022-10-12T12:00+03:00',
    'tag': 'Orange',
    'message': 'test_message',
    'end_time': '2022-12-12T12:00+03:00'
}

ml_payload_with_id = {'ml_id': 1}
ml_payload_with_id.update(ml_payload)

login_payload = {
    'username': 'Ciremun',
    'password': 'sFlhF^ush*5Xnhj'
}

bad_login_payload = {
    'login': 'test'
}

headers = {
    'Content-Type': 'application/json'
}

cookies = {
    'SID': None
}

localhost = f'http://localhost:{int(os.environ.get("PORT"))}'


class TestEndpoints(unittest.TestCase):

    def test_a_add_client_no_auth(self):
        r = rq.post(f'{localhost}/add_client',
                    json=client_payload, headers=headers).json()
        self.assertEqual(r['status'], 401)

    def test_b_update_client_no_auth(self):
        r = rq.put(f'{localhost}/update_client',
                   json=client_payload_with_id, headers=headers).json()
        self.assertEqual(r['status'], 401)

    def test_c_delete_client_no_auth(self):
        r = rq.delete(f'{localhost}/delete_client/1', headers=headers).json()
        self.assertEqual(r['status'], 401)

    def test_d_add_mailing_list_no_auth(self):
        r = rq.post(f'{localhost}/add_mailing_list',
                    json=ml_payload, headers=headers).json()
        self.assertEqual(r['status'], 401)

    def test_e_update_mailing_list_no_auth(self):
        r = rq.put(f'{localhost}/update_mailing_list',
                   json=ml_payload_with_id, headers=headers).json()
        self.assertEqual(r['status'], 401)

    def test_f_delete_mailing_list_no_auth(self):
        r = rq.delete(f'{localhost}/delete_mailing_list/1',
                      headers=headers).json()
        self.assertEqual(r['status'], 401)

    def test_g_mailing_list_stats_no_auth(self):
        r = rq.get(f'{localhost}/mailing_list_stats', headers=headers).json()
        self.assertEqual(r['status'], 401)

    def test_h_mailing_list_details_no_auth(self):
        r = rq.get(f'{localhost}/mailing_list_details/1',
                   headers=headers).json()
        self.assertEqual(r['status'], 401)

    def test_i_login(self):
        r = rq.post(f'{localhost}/login',
                    json=bad_login_payload, headers=headers).json()
        self.assertEqual(r['status'], 400)
        r = rq.post(f'{localhost}/login', json=login_payload,
                    headers=headers).json()
        self.assertEqual(r['status'], 200)
        cookies['SID'] = r.get('cookie')[1]

    def test_j_add_client(self):
        r = rq.post(f'{localhost}/add_client', json=client_payload,
                    headers=headers, cookies=cookies).json()
        self.assertEqual(r['status'], 200)
        self.assertIsNotNone(r.get('client_id', None))

    def test_k_update_client(self):
        r = rq.put(f'{localhost}/update_client', json=client_payload_with_id,
                   headers=headers, cookies=cookies).json()
        self.assertEqual(r['status'], 200)

    def test_l_delete_client(self):
        r = rq.delete(f'{localhost}/delete_client/1',
                      headers=headers, cookies=cookies).json()
        self.assertEqual(r['status'], 200)

    def test_m_add_mailing_list(self):
        r = rq.post(f'{localhost}/add_mailing_list',
                    json=ml_payload, headers=headers, cookies=cookies).json()
        self.assertEqual(r['status'], 200)
        self.assertIsNotNone(r.get('mailing_list_id', None))

    def test_n_update_mailing_list(self):
        r = rq.put(f'{localhost}/update_mailing_list',
                   json=ml_payload_with_id, headers=headers, cookies=cookies).json()
        self.assertEqual(r['status'], 200)

    def test_o_delete_mailing_list(self):
        r = rq.delete(f'{localhost}/delete_mailing_list/1',
                      headers=headers, cookies=cookies).json()
        self.assertEqual(r['status'], 200)

    def test_p_mailing_list_stats(self):
        r = rq.get(f'{localhost}/mailing_list_stats',
                   headers=headers, cookies=cookies).json()
        self.assertEqual(r['status'], 200)

    def test_q_mailing_list_details(self):
        r = rq.get(f'{localhost}/mailing_list_details/1',
                   headers=headers, cookies=cookies).json()
        self.assertEqual(r['status'], 200)

    def test_r_logout(self):
        r = rq.post(f'{localhost}/logout', headers=headers)
        self.assertEqual(r.status_code, 200)

    @classmethod
    def tearDownClass(cls):
        cursor.execute('DROP DATABASE ml_test;')
        cursor.close()
        conn.close()


if __name__ == '__main__':
    api_thread = Thread(target=main, daemon=True)
    api_thread.start()
    while rq.get(f'{localhost}/docs').status_code != 200:
        print('waiting for server')
    unittest.main()
