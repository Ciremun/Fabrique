#!/usr/bin/env python3

if __name__ != '__main__':
    exit(1)

import sys

if len(sys.argv) < 2:
    print('Usage: ./add_admin.py [username]')
    exit(1)

import os
import hashlib
import binascii
from datetime import datetime

import psycopg2
from dotenv import load_dotenv

load_dotenv()

def hash_password(password: str) -> str:
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')

conn = psycopg2.connect(os.environ.get('DATABASE_URL'))
conn.autocommit = True
cursor = conn.cursor()

username = sys.argv[1]
cursor.execute(
    'SELECT exists(SELECT 1 FROM users WHERE username = %s)', (username,))
user_exists = cursor.fetchone()[0]
if user_exists:
    cursor.execute(
        "UPDATE users SET usertype = 'admin' WHERE username = %s", (username,))
else:
    password_plain_text = input('password?\n')
    hashed_pwd = hash_password(password_plain_text)
    cursor.execute("INSERT INTO users (username, password, usertype, date) VALUES (%s, %s, %s, %s)", (username, hashed_pwd, 'admin', datetime.now()))
