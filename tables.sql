CREATE TABLE IF NOT EXISTS 
users(id serial primary key, username text, password text, usertype text, date timestamp);

CREATE TABLE IF NOT EXISTS 
session(id serial primary key, sid text, user_id integer references users(id) on delete cascade, date timestamp);

CREATE TABLE IF NOT EXISTS 
client(id serial primary key, phone_number text, operator_code integer, tag text, timezone text);

CREATE TABLE IF NOT EXISTS 
ml(id serial primary key, start_time timestamp with time zone, message text, tag text, end_time timestamp with time zone);

CREATE TABLE IF NOT EXISTS 
message(id serial primary key, date timestamp with time zone, status integer, ml_id integer references ml(id) on delete cascade, client_id integer references client(id) on delete cascade);
