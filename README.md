
# Notification Service

## Дополнительные задания

Предположительно выполнены пункты:  

1, 2, 3, 5, 8, 9, 12

## Установка (Linux)

На примере Ubuntu версии 18.04/20.04

### PostgreSQL

    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get -y install postgresql
    sudo /etc/init.d/postgresql start

### Создание базы данных и добавление пароля

    sudo -i -u postgres
    psql
    create database ml;
    alter user postgres password 'password';
    \q
    exit

### Python

На многих системах Python уже установлен, можно проверить командой:

    python3 --version

Требуется Python версии 3.7 или выше.

Если Python не установлен:

    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt update
    sudo apt install -y python3.8

Далее установить зависимости, находясь в папке проекта:

    sudo apt install -y python3-pip
    python3 -m pip install -r requirements.txt

### Docker

[Официальная Документация по Установке](https://docs.docker.com/engine/install/ubuntu/)  

### Переменные окружения

Создать файл с переменными окружения:


|      Key                 |                Value                                     |
|--------------------------|----------------------------------------------------------|
| `PORT`                   | Application Port                                         |
| `DATABASE_URL`           | PostgreSQL [connection URI](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING) |
| `SECRET_KEY`             | Password / Session Encryption Key                        |
| `YANDEX_LOGIN`           | Notification Sender e-mail                               |
| `YANDEX_OAUTH2_TOKEN`    | Notification Sender [OAuth2 Token](https://yandex.ru/dev/id/doc/dg/oauth/reference/web-client.html)|
| `TARGET_MAIL`            | Notification Recipient                                   |
| `JWT_TOKEN`              | Fabrique JWT Token                                       |

## Запуск

Продакшн:

    export $(cat .env)
    python3 app.py

Отладка:

    export FLASK_DEBUG=1
    python3 -m flask run

## docker-compose

    sudo /etc/init.d/docker start
    sudo docker compose up

## Регистрация

Ввести имя пользователя и пароль, новый пользователь будет создан автоматически.

## Добавление администратора

    python3 tools/set_admin.py [Имя пользователя]

## Тестирование

    python3 test.py

### Запуск CI локально

    gitlab-runner exec shell unit-test-job
