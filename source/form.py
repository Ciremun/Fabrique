from enum import Enum
from typing import List, Optional, NamedTuple

from flask import flash, redirect, make_response, abort, jsonify


class Cookie(NamedTuple):
    key: str
    value: str


class ResponseTypeError(Exception):
    pass


class ResponseType(Enum):
    HTTP = 1
    JSON = 2


class FormHandler:

    def __init__(self, request, session=None, redirect_url='/', response_type=ResponseType.HTTP):
        self.request = request
        self.redirect_url = redirect_url
        self.response_type = response_type
        self.session = session
        self.message = None

    def make_response(self, **data):
        if self.response_type == ResponseType.JSON:
            response_data = jsonify(**data)
        elif self.response_type == ResponseType.HTTP:
            if 'message' in data:
                flash(data['message'])
            response_data = redirect(self.redirect_url)
        else:
            raise ResponseTypeError(
                f'Unknown ResponseType: {self.response_type}')
        response = make_response(response_data)
        if data.get('cookie'):
            response.set_cookie(
                data['cookie'].key, data['cookie'].value, max_age=2620000, secure=self.request.is_secure)
        return response

    def get_form(self):
        message = self.request.form
        if not message:
            if all(self.request.method != method for method in ('GET', 'DELETE')):
                message = self.request.get_json()
                if not message:
                    abort(400)
            self.response_type = ResponseType.JSON
        self.message = message

    def get_list(self, key: str) -> Optional[List]:
        if self.response_type == ResponseType.JSON:
            return self.message.get(key)
        elif self.response_type == ResponseType.HTTP:
            return self.message.getlist(key)
        else:
            raise ResponseTypeError(
                f'Unknown ResponseType: {self.response_type}')
