from enum import IntEnum

class MessageStatus(IntEnum):
    CREATED = 0,
    SENT = 1
