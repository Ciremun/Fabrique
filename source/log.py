from logging.handlers import RotatingFileHandler
from pathlib import Path
import logging
import sys

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
Path("log/").mkdir(exist_ok=True)

streamHandlerFormatter = logging.Formatter('%(message)s')
streamHandler = logging.StreamHandler(sys.stdout)
streamHandler.setFormatter(streamHandlerFormatter)
logger.addHandler(streamHandler)

fileHandlerFormatter = logging.Formatter(
    '[%(asctime)s] %(levelname)s:%(message)s')
fileHandler = RotatingFileHandler(
    'log/latest.log', mode='a', maxBytes=5242880, backupCount=1, encoding='utf-8')
fileHandler.setFormatter(fileHandlerFormatter)
logger.addHandler(fileHandler)

def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

sys.excepthook = handle_exception
