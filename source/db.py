import os
from functools import wraps
from datetime import datetime
from typing import List, Tuple

import psycopg2

from source.message import MessageStatus

conn = psycopg2.connect(os.environ.get('DATABASE_URL'))
conn.autocommit = True


def db(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        with conn.cursor() as c:
            return f(c, *args, **kwargs)
    return wrapper


@db
def db_init(c):
    c.execute(open('tables.sql').read())


@db
def add_client(c, phone_number: str, operator_code: int, tag: str, timezone: str) -> Tuple[int]:
    sql = """\
INSERT INTO client (phone_number, operator_code, tag, timezone) 
VALUES (%s, %s, %s, %s) RETURNING id\
"""
    c.execute(sql, (phone_number, operator_code, tag, timezone))
    return c.fetchone()[0]


@db
def update_client(c, phone_number: str, operator_code: int, tag: str, timezone: str, client_id: int):
    sql = """\
UPDATE client SET phone_number = %s, operator_code = %s, tag = %s, timezone = %s 
WHERE id = %s\
"""
    c.execute(sql, (phone_number, operator_code, tag, timezone, client_id))


@db
def delete_client(c, client_id: int):
    sql = """\
DELETE FROM client WHERE id = %s\
"""
    c.execute(sql, (client_id,))


@db
def add_mailing_list(c, start_time: datetime, message: str, tag: str, end_time: datetime) -> Tuple[int]:
    sql = """\
INSERT INTO ml (start_time, message, tag, end_time) 
VALUES (%s, %s, %s, %s) RETURNING id\
"""
    c.execute(sql, (start_time, message, tag, end_time))
    return c.fetchone()[0]


@db
def get_mailing_list(c) -> List[Tuple[int, datetime, str, str, datetime]]:
    sql = """\
SELECT id, start_time, message, tag, end_time FROM ml\
"""
    c.execute(sql)
    return c.fetchall()


@db
def get_tagged_clients(c, tag: str) -> List[Tuple[int, str, int, str]]:
    sql = """\
SELECT id, phone_number, operator_code, timezone FROM client 
WHERE tag = %s\
"""
    c.execute(sql, (tag,))
    return c.fetchall()

@db
def mailing_list_stats(c) -> List[Tuple]:
    sql = """\
SELECT ml.id, COUNT(m), m.status FROM ml \
JOIN message m ON m.ml_id = ml.id \
GROUP BY ml.id, m.status\
"""
    c.execute(sql)
    return c.fetchall()


@db
def mailing_list_details(c, ml_id: int) -> List[Tuple[int, datetime, MessageStatus, str]]:
    sql = """\
SELECT ml.id, m.date, m.status, ml.tag FROM ml \
JOIN message m ON m.ml_id = ml.id \
WHERE ml.id = %s
\
"""
    c.execute(sql, (ml_id,))
    return c.fetchall()


@db
def update_mailing_list(c, start_time: datetime, message: str, tag: str, end_time: datetime, ml_id: int):
    sql = """\
UPDATE ml SET start_time = %s, message = %s, tag = %s, end_time = %s 
WHERE id = %s\
"""
    c.execute(sql, (start_time, message, tag, end_time, ml_id))


@db
def delete_mailing_list(c, ml_id: int):
    sql = """\
DELETE FROM ml WHERE id = %s\
"""
    c.execute(sql, (ml_id,))


@db
def add_message(c, date: datetime, status: MessageStatus, ml_id: int, client_id: int) -> Tuple[int]:
    sql = """\
INSERT INTO message (date, status, ml_id, client_id) 
VALUES (%s, %s, %s, %s) RETURNING id\
"""
    c.execute(sql, (date, status, ml_id, client_id))
    return c.fetchone()[0]


@db
def update_message(c, date: datetime, status: MessageStatus, ml_id: int, client_id: int, message_id: int):
    sql = """\
UPDATE message SET date = %s, status = %s, ml_id = %s, client_id = %s 
WHERE id = %s
"""
    c.execute(sql, (date, status, ml_id, client_id, message_id))


@db
def add_session(c, session_id: str, user_id: int, date: datetime):
    sql = """\
INSERT INTO session (sid, user_id, date) \
VALUES (%s, %s, %s)\
"""
    c.execute(sql, (session_id, user_id, date))


@db
def get_session(c, session_id: str):
    sql = """\
SELECT u.username, u.usertype, s.user_id, s.date \
FROM session s JOIN users u ON u.id = s.user_id WHERE sid = %s\
"""
    c.execute(sql, (session_id,))
    return c.fetchone()


@db
def update_session_date(c, session_id: str, newdate: datetime):
    sql = "UPDATE session SET date = %s WHERE sid = %s"
    c.execute(sql, (newdate, session_id))


@db
def delete_session(c, sid: str):
    sql = "DELETE FROM session WHERE sid = %s"
    c.execute(sql, (sid,))


@db
def add_user(c, username: str, password: str, usertype: str, date: datetime):
    sql = "INSERT INTO users (username, password, usertype, date) VALUES (%s, %s, %s, %s) RETURNING id"
    c.execute(sql, (username, password, usertype, date))
    return c.fetchone()[0]


@db
def get_user(c, username: str):
    sql = "SELECT id, password, usertype FROM users WHERE username = %s"
    c.execute(sql, (username,))
    return c.fetchone()


db_init()
