import os
import datetime
import hashlib
import binascii
from typing import Optional, NamedTuple
from functools import wraps

from flask import request
from pytz import UnknownTimeZoneError

import source.db as db
from source.form import FormHandler
from source.log import logger


class Session(NamedTuple):
    id: str
    username: str
    usertype: str
    user_id: int
    date: datetime.datetime


def update_user_session(session: Session):
    now = datetime.datetime.now()
    day = datetime.timedelta(days=1)
    if session.date + day < now:
        db.update_session_date(session.id, now)


def get_session(session_id: str) -> Optional[Session]:
    if not isinstance(session_id, str):
        return None
    s = db.get_session(session_id)
    if s:
        username, usertype, user_id, date = s
        session = Session(session_id, username, usertype, user_id, date)
        update_user_session(session)
        return session
    return None


def authorized_endpoint(route):
    @wraps(route)
    def wrapper(*args, **kwargs):
        session_id = request.cookies.get('SID')
        session = get_session(session_id)
        handler = FormHandler(request, session=session, redirect_url='/login')
        handler.get_form()
        if session is None or session.usertype != 'admin':
            return handler.make_response(message='Ошибка: требуется авторизация', status=401)
        else:
            try:
                result = route(handler, *args, **kwargs)
                logger.info(f'Success at /{request.endpoint}: {handler.message}')
                return result;
            except UnknownTimeZoneError as e:
                logger.info(f'UnknownTimeZoneError at /{request.endpoint}: {type(e).__name__}, {e}')
                return handler.make_response(message='Ошибка: неизвестный часовой пояс', status=400)
            except Exception as e:
                logger.info(f'Bad Request at /{request.endpoint}: {type(e).__name__}, {e}')
                return handler.make_response(message='Ошибка: некорректный запрос', status=400)
    return wrapper


def optional_session(route):
    @wraps(route)
    def wrapper(*args, **kwargs):
        session_id = request.cookies.get('SID')
        session = get_session(session_id)
        return route(session, *args, **kwargs)
    return wrapper


def hash_password(password):
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def verify_password(stored_password, provided_password):
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    pwdhash = hashlib.pbkdf2_hmac('sha256', provided_password.encode('utf-8'),
                                  salt.encode('ascii'), 100000)
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password
