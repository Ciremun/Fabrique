from pytz import timezone

from source.form import FormHandler

def verify_client_and_get_operator_code(handler: FormHandler) -> int:
    timezone(handler.message['timezone'])
    assert(isinstance(handler.message['phone_number'], str))
    assert(handler.message['phone_number'].isdigit())
    assert(len(handler.message['phone_number']) == 11)
    return int(handler.message['phone_number'][1:4])
