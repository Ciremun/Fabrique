import smtplib
import base64
import os

from source.log import logger

login = os.environ.get('YANDEX_LOGIN', '').strip()
token = os.environ.get('YANDEX_OAUTH2_TOKEN', '').strip()

oauth2_b64encoded = base64.b64encode(f'user={login}\x01auth=Bearer {token}\x01\x01'.encode()).decode()

def send_mail(recipient: str, subject: str, message: str):
    conn = smtplib.SMTP_SSL('smtp.yandex.com:465')
    conn.command_encoding = 'utf-8'
    logger.info(conn.ehlo())
    logger.info(conn.docmd(f'AUTH XOAUTH2 {oauth2_b64encoded}'))
    logger.info(conn.docmd(f'MAIL FROM:<{login}>'))
    logger.info(conn.docmd(f'rcpt to:<{recipient}>'))
    logger.info(conn.docmd('DATA'))
    logger.info(conn.docmd(f'From: "notification-service" <{login}>\r\nTo: "Name" <{recipient}>\r\nSubject:{subject}\r\n\r\n{message}\r\n.\r\n'))
    conn.close()
