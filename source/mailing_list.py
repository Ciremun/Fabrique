import os
import time
import json
from datetime import datetime
from functools import wraps

import requests

import source.db as db
from source.form import FormHandler
from source.message import MessageStatus
from source.smtp import send_mail
from source.log import logger

jwt_token = os.environ.get('JWT_TOKEN', '').strip()


def exponential_backoff(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        for exponent in range(1, 5):
            try:
                return f(*args, **kwargs)
            except Exception as e:
                logger.warning(f"Couldn't send message: {type(e).__name__}, {e}")
                time.sleep(5 ** exponent)
    return wrapper


@exponential_backoff
def send_message(message_id: int, client_id: int, phone_number: str, message: str):
    if not jwt_token:
        logger.warning('JWT_TOKEN is not set')
        return False
    json = {
        "id": client_id,
        "phone": int(phone_number),
        "text": message
    }
    headers = {
        'Authorization': f'Bearer {jwt_token}'
    }
    logger.info(f'Send JSON: {json}')
    response = requests.post(
        f'https://probe.fbrq.cloud/v1/send/{message_id}', json=json, headers=headers)
    logger.info(f'Response: {response.status_code}, {response.text}')
    return True


# TODO(Ciremun): come up with an asynchronous way to process the messages
def mailing_list_handler():
    while True:
        now = datetime.now()
        for ml_id, start_time, message, tag, end_time in db.get_mailing_list():
            start_time_now = datetime.now(start_time.tzinfo)
            end_time_now = datetime.now(end_time.tzinfo)
            if (start_time < start_time_now) and (end_time_now < end_time):
                for client_id, phone_number, _, _ in db.get_tagged_clients(tag):
                    message_id = db.add_message(now, MessageStatus.CREATED,
                                                ml_id, client_id)
                    logger.info(
                        f'Send Message: message_id={message_id}, ml_id={ml_id}, client_id={client_id}, message={message}, tag={tag}')
                    if (start_time < start_time_now) and (end_time_now < end_time):
                        if send_message(message_id, client_id,
                                        phone_number, message):
                            db.update_message(
                                now, MessageStatus.SENT, ml_id, client_id, message_id)
                    else:
                        break
        time.sleep(60 * 60 * 24)


def notification_handler():
    mail_recepient = os.environ.get('TARGET_MAIL', '').strip()
    if not mail_recepient:
        logger.warning(
            'exiting notification_handler thread: TARGET_MAIL is not set')
        return
    while True:
        mailing_list_stats = db.mailing_list_stats()
        if mailing_list_stats:
            stats = []
            for ml_id, messages_count, message_group in mailing_list_stats:
                stats.append({'mailing_list_id': ml_id, 'messages_count': messages_count,
                             'message_group': str(MessageStatus(message_group))})
            stats = json.dumps(stats, indent=4)
            send_mail(mail_recepient, 'mailing list stats', stats)
            logger.info(
                f'notification_handler: send mailing list stats - {stats}')
        time.sleep(60 * 60 * 24)


def verify_mailing_list(handler: FormHandler):
    handler.message['start_time'] = datetime.fromisoformat(
        handler.message['start_time'])
    handler.message['end_time'] = datetime.fromisoformat(
        handler.message['end_time'])
    assert(handler.message['start_time'] < handler.message['end_time'])
